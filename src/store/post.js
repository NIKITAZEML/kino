import PostService from "@/services/PostServices.js"

export default { 
    namespaced: true,           // Стандартный набор vuex  
    state:() => ({
        post: null,
        posts: [],
    }),

    actions: {
        async getAll(ctx){
           const posts = await PostService.getAll(); // Ключевой момент. Включаем всё, что мы наделали
           console.log(posts);
           ctx.commit('setPosts', posts)
        },


        async create(){
            const post = await PostService.create({
                title: "My New Object",
                body: "Body"
            }); // Ключевой момент. Включаем всё, что мы наделали
            console.log(post);
         },
    },

    mutations:{
        setPosts: (state, payload) => state.posts = payload,
        setPost: (state, payload) => state.post = payload,
    },

    getters:{
        getPosts: state => state.posts,
        getPost: state => state.post
    }
}