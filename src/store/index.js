import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import post from "@/store/post.js"

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    post
  }
})
