class PostServices { // создали класс по названию файла в котором будут какие-то методы
   async getAll(){

        // new Promise( (resolve, reject) => { // если всё четко - попадаем в блок then. За это отвечает resolve
        //     resolve                         // если всё плохо - попадаем в catch. За это отвечает reject
        // } ).then( () => {

        // } ).catch( ()=>{

        // } )



    // fetch( "https://jsonplaceholder.typicode.com/posts?_limit=50" )
    //     .then( (res) => { return res.json()})       //  res - название переменной, содержащей список объектов из json 
    //     .then( (posts) => {console.log(posts)} )     // возвращаем наш список методом json() (парсим их в js объект)
         // В этом способе асинк перед getAll() не нужен 

        const data = await fetch( "https://jsonplaceholder.typicode.com/photos?_limit=50/" );
        const res = await data.json()
        return res 
    }

}

export default new PostServices() // Экспортировали чтобы юзать в любом компоненте 